use std::fmt;
use serde::{Serialize, Deserialize};
use serde_json::value::{Value, Number};
//use serde::de::IgnoredAny;

#[derive(Debug)]
pub struct RealtorApiRequest {
    pub host: String,
    pub endpoint: String,
    pub query: RealtorApiQuery,
}
  
#[derive(Serialize, Debug)]
pub struct RealtorApiQuery {
    pub api_key: String,
    pub city: Option<String>,
    pub state_code: Option<String>,
    pub limit: u64,
    pub offset: u64,
    pub postal_code: Option<String>,
    pub radius: Option<u64>,
    pub price_min: Option<f32>,
    pub price_max: Option<f32>,
    pub sqft_min: Option<u64>,
    pub sqft_max: Option<u64>,
    pub lot_sqft_min: Option<u64>,
    pub lot_sqft_max: Option<u64>,
    pub baths_min: Option<u64>,
    pub beds_min: Option<u64>,
    pub sort: String,    //consider an enum
    pub allows_cats: Option<bool>,
    pub allows_dogs: Option<bool>,
    pub features: Option<String>,
    pub prop_type: Option<String>
}

impl RealtorApiQuery {
    pub fn builder() -> RealtorApiQueryBuilder {
        RealtorApiQueryBuilder {
            api_key: String::from(""),
            city: None,
            state_code: None,
            limit: u64::MAX,
            offset: 0,
            postal_code: None,
            radius: None,
            price_min: None,
            price_max: None,
            sqft_min: None,
            sqft_max: None,
            lot_sqft_min: None,
            lot_sqft_max: None,
            baths_min: None,
            beds_min: None,
            sort: String::from("newest"),
            allows_cats: None,
            allows_dogs: None,
            features: None,
            prop_type: None
        }
    }

    pub fn increment_offset(&mut self, increment: u64) {
        self.offset += increment;
    }
}

pub struct RealtorApiQueryBuilder {
    pub api_key: String,
    pub city: Option<String>,
    pub state_code: Option<String>,
    pub limit: u64,
    pub offset: u64,
    pub postal_code: Option<String>,
    pub radius: Option<u64>,
    pub price_min: Option<f32>,
    pub price_max: Option<f32>,
    pub sqft_min: Option<u64>,
    pub sqft_max: Option<u64>,
    pub lot_sqft_min: Option<u64>,
    pub lot_sqft_max: Option<u64>,
    pub baths_min: Option<u64>,
    pub beds_min: Option<u64>,
    pub sort: String,
    pub allows_cats: Option<bool>,
    pub allows_dogs: Option<bool>,
    pub features: Option<String>,
    pub prop_type: Option<String>
}

impl RealtorApiQueryBuilder {
    pub fn build(self) -> RealtorApiQuery {
        RealtorApiQuery {
            api_key: self.api_key,
            city: self.city,
            state_code: self.state_code,
            limit: self.limit,
            offset: self.offset,
            postal_code: self.postal_code,
            radius: self.radius,
            price_min: self.price_min,
            price_max: self.price_max,
            sqft_min: self.sqft_min,
            sqft_max: self.sqft_max,
            lot_sqft_min: self.lot_sqft_min,
            lot_sqft_max: self.lot_sqft_max,
            baths_min: self.baths_min,
            beds_min: self.beds_min,
            sort: self.sort,
            allows_cats: self.allows_cats,
            allows_dogs: self.allows_dogs,
            features: self.features,
            prop_type: self.prop_type
        }
    }

    pub fn api_key(mut self, api_key: &str) -> Self {
        self.api_key = String::from(api_key);
        self
    }

    pub fn city(mut self, city: &str) -> Self {
        //api cannot use postal code and city at once
        if let Some(_) = self.postal_code {
            self.postal_code = None;
        }
        self.city = Some(String::from(city));
        self
    }

    pub fn state_code(mut self, state_code: &str) -> Self {
        //api cannot use postal code and state_code at once
        if let Some(_) = self.postal_code {
            self.postal_code = None;
        }
        self.state_code = Some(String::from(state_code));
        self
    }

    pub fn limit(mut self, limit: u64) -> Self {
        self.limit = limit;
        self
    }


    pub fn postal_code(mut self, postal_code: &str) -> Self {
        //api cannot use postal code and city or state_code at once
        if let Some(_) = self.city {
            self.city = None;
        }
        if let Some(_) = self.state_code {
            self.state_code = None;
        }

        self.postal_code = Some(String::from(postal_code));
        self
    }

    pub fn radius(mut self, radius: u64) -> Self {
        self.radius = Some(radius);
        self
    }

    pub fn price_min(mut self, price_min: f32) -> Self {
        self.price_min = Some(price_min);
        self
    }

    pub fn price_max(mut self, price_max: f32) -> Self {
        self.price_max = Some(price_max);
        self
    }

    pub fn sqft_min(mut self, sqft_min: u64) -> Self {
        self.sqft_min = Some(sqft_min);
        self
    }

    pub fn sqft_max(mut self, sqft_max: u64) -> Self {
        self.sqft_max = Some(sqft_max);
        self
    }

    pub fn lot_sqft_min(mut self, lot_sqft_min: u64) -> Self {
        self.lot_sqft_min = Some(lot_sqft_min);
        self
    }

    pub fn lot_sqft_max(mut self, lot_sqft_max: u64) -> Self {
        self.lot_sqft_max = Some(lot_sqft_max);
        self
    }

    pub fn baths_min(mut self, baths_min: u64) -> Self {
        self.baths_min = Some(baths_min);
        self
    }

    pub fn beds_min(mut self, beds_min: u64) -> Self {
        self.beds_min = Some(beds_min);
        self
    }

    pub fn sort(mut self, sort: &str) -> Self {
        self.sort = String::from(sort);
        self
    }

    pub fn allows_cats(mut self, allows_cats: bool) -> Self {
        self.allows_cats = Some(allows_cats);
        self
    }

    pub fn allows_dogs(mut self, allows_dogs: bool) -> Self {
        self.allows_dogs = Some(allows_dogs);
        self
    }

    pub fn features(mut self, features: &str) -> Self {
        self.features = Some(String::from(features));
        self
    }

    pub fn prop_type(mut self, prop_type: &str) -> Self {
        self.prop_type = Some(String::from(prop_type));
        self
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RealtorApiResponse {
    pub meta: Meta,
    pub properties: Option<Vec<Property>>
}
#[derive(Serialize, Deserialize, Debug)]
pub struct Meta {
    pub returned_rows: Number,
    pub matching_rows: Number
}
#[derive(Serialize, Deserialize, Debug)]
pub struct Property {
    pub address: Address,
    pub beds: Option<Number>,
    pub baths: Option<Number>,
    pub rdc_web_url: Option<String>,
    pub community: Option<Community>,
    pub price: Option<Number>
}

impl fmt::Display for Property {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let address = format!("{}, {}, {} {}", 
                              self.address.line.as_ref().unwrap_or(&String::from("N/A")), 
                              self.address.city.as_ref().unwrap_or(&String::from("N/A")), 
                              self.address.state_code.as_ref().unwrap_or(&String::from("N/A")), 
                              self.address.postal_code.as_ref().unwrap_or(&String::from("N/A")));
        let price: String;
        let beds: String;
        let baths: String;

        if let Some(comm) = &self.community {
           price = format!("(Community) {}", Community::format_range(comm.price_min.as_ref(), comm.price_max.as_ref(), Some(String::from("$"))));
           beds = format!("(Community) {}", Community::format_range(comm.beds_min.as_ref(), comm.beds_max.as_ref(), None));
           baths = format!("(Community) {}", Community::format_range(comm.baths_min.as_ref(), comm.baths_max.as_ref(), None));
        }
        else {
            price = match &self.price {
                Some(num) => format!("${}", num.to_string()),
                None => String::from("N/A"),
            };
            beds = match &self.beds {
                Some(num) => num.to_string(),
                None => String::from("N/A"),
            };
            baths = match &self.baths {
                Some(num) => num.to_string(),
                None => String::from("N/A"),
            };
        }

        write!(f, "address: {}\nprice: {}\nbeds: {}\nbaths: {}\nurl: {}\n",
               address,
               price,
               beds,
               baths,
               self.rdc_web_url.as_ref().unwrap_or(&String::from("N/A")))
    }

}

#[derive(Serialize, Deserialize, Debug)]
pub struct Address {
    pub city: Option<String>,
    pub state: Option<String>,
    pub state_code: Option<String>,
    pub country: Option<String>,
    pub county: Option<String>,
    pub line: Option<String>, //address line 1
    pub neighborhood_name: Option<String>,
    pub neighborhoods: Option<Value>,
    pub postal_code: Option<String>,
    pub time_zone: Option<String>
}
#[derive(Serialize, Deserialize, Debug)]
pub struct Community {
    pub price_min: Option<Number>,
    pub price_max: Option<Number>,
    pub beds_min: Option<Number>,
    pub beds_max: Option<Number>,
    pub baths_min: Option<Number>,
    pub baths_max: Option<Number>,
    pub sqft_min: Option<Number>,
    pub sqft_max: Option<Number>
}

impl Community {
    pub fn format_range(min: Option<&Number>, max: Option<&Number>, prefix: Option<String>) -> String {
        let prefix = prefix.unwrap_or_else(|| String::new());
        
        if min.is_none() || max.is_none() {
            if let Some(m) = min {
                return format!("{pre}{num}", pre=prefix, num=m.to_string());
            }
            else if let Some(m) = max {
                return format!("{pre}{num}", pre=prefix, num=m.to_string());
            }
            else {
                return String::from("N/A");
            }
        }

        if min.unwrap() == max.unwrap() {
            return format!("{pre}{num}", pre=prefix, num=min.unwrap().to_string());
        }

        format!("{pre}{min} - {pre}{max}", pre=prefix, min=min.unwrap().to_string(), max=max.unwrap().to_string())
    }
}
