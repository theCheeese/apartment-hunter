use lettre::transport::smtp::authentication::Credentials;
use lettre::{Transport, SmtpTransport, Message};
use crate::config::EmailSettings;

pub fn email(config: EmailSettings, body: &str) -> std::result::Result<bool, lettre::transport::smtp::Error> {
    let credentials = Credentials::new(String::from(&config.email_sender), String::from(config.email_sender_pass));
    let mailer = SmtpTransport::relay(config.email_smtp.as_str())
        .unwrap()
        .credentials(credentials)
        .build();

    let subject = &config.email_sender_subject.unwrap_or(String::from("apartment-hunter found some places for you!"));

    for recipient in config.email_recipients {
        let email = Message::builder()
            .from(format!("apartment-hunter <{}>", &config.email_sender).parse().unwrap())
            .to(recipient.email.parse().unwrap())
            .subject(subject)
            .body(String::from(body))
            .unwrap();

        match mailer.send(&email) {
            Err(err) => return Err(err),
            _ => (),
        };
    }
    Ok(true)
}
