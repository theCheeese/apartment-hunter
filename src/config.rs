use std::fs::File;
use std::io::{BufReader, Read};
use serde::{Deserialize};


#[derive(Debug, Deserialize)]
pub struct Settings {
    pub api_key: String,
    pub email_settings: EmailSettings,
}

impl Settings {
    pub fn from_filepath(path: &str) -> Settings {
        let file = File::open(path).expect(format!("Could not open the config file at {}", path).as_str());
        let mut reader = BufReader::new(file);
        let mut contents = String::new();
        if let Err(e) = reader.read_to_string(&mut contents) {
            panic!("Failed to read the config file at {}: {}", path, e);
        }

        serde_json::from_str::<Settings>(contents.as_str()).unwrap_or_else(|e| panic!("Failed to parse the config file at {}: {}", path, e))
    }
}

#[derive(Debug, Deserialize)]
pub struct EmailSettings {
    pub email_recipients: Vec<Email>,
    pub email_sender: String,
    pub email_sender_pass: String,
    pub email_sender_subject: Option<String>,
    pub email_smtp: String,
}

#[derive(Debug, Deserialize)]
pub struct Email {
    pub email: String,
}
