use std::cmp;
use std::iter::Iterator;
use reqwest::blocking::Client;
use clap::{App, ArgMatches, Arg};
extern crate serde_qs as qs;

mod realtor;
use crate::realtor::{RealtorApiQuery, RealtorApiResponse, Property};
mod config;
use crate::config::Settings;
mod notification_methods;
use crate::notification_methods as notifs;

fn main() { 
    let args = App::new("apartment-hunter")
        .version("0.1")
        .about(include_str!("../README.md"))
        .arg("--email 'Setting this will make apartment-hunter email the results to the recipients in the config file once the run is complete'")
        .arg("--json 'Setting this will make apartment-hunter output the results to stdout in json format'")
        .arg("--allows-cats=[ALLOWS_CATS] 'One of true/false'")
        .arg("--allows-dogs=[ALLOWS_DOGS] 'One of true/false'")
        .arg("--city=[CITY] 'The city to search. Cannot be used with --zip.'")
        .arg(Arg::new("features")
             .long("features")
             .multiple(true)
             .value_name("FEATURES")
             .about("--features=[FEATURES] 'Filter to one or more property features: recreation_facilities, swimming_pool, washer_dryer, garage_1_or_more, central_air, fireplace, spa_or_hot_tub, dishwasher, community_doorman, community_elevator, furnished, laundry_room, community_no_fee, community_outdoor_space, pets_allowed'"))
        .arg("--limit=[LIMIT] 'Limit number of results to [LIMIT]. Default all.'")
        .arg("--min-price=[MIN_PRICE] 'The minimum acceptable price.'")
        .arg("--max-price=[MAX_PRICE] 'The maximum acceptable price.'")
        .arg("--min-sqft=[MIN_SQFT] 'The minimum acceptable area of the property'")
        .arg("--max-sqft=[MAX_SQFT] 'The maximum acceptable area of the property'")
        .arg("--min-lot-sqft=[MIN_LOT_SQFT] 'The minimum acceptable area of the lot'")
        .arg("--max-lot-sqft=[MAX_LOT_SQFT] 'The maximum acceptable area of the lot'")
        .arg("--min-baths=[MIN_BATHS] 'The minimum acceptable number of bathrooms'")
        .arg("--min-beds=[MIN_BEDS] 'The minimum acceptable number of bedrooms.'")
        .arg(Arg::new("prop-type")
             .long("prop-type")
             .multiple(true)
             .value_name("PROP_TYPE")
             .about("Filter to one or more of the following property types: single_family, multi_family, condo, mobile, land, farm, other"))
        .arg("--radius=[RADIUS] 'The acceptable radius around the input location in miles.'")
        .arg("--sort=[SORT] 'Sort results by one of: relevance, newest, price_low, price_high, photos, open_house_date, sqft_high, price_reduced_date'")
        .arg("--state=[STATE] 'The state to search e.g. NY. Cannot be used with --zip.'")
        .arg("--zip=[ZIP] 'The zip code to search. Cannot be used with --city or --state.'")
        .arg("<CONFIG> 'Sets the json configuration file that to use.'")
        .get_matches();

    let config = load_config(&args);
    let query = build_query(&args, &config);
    let properties = get_properties(query);
    notify_recipients(args, config, properties);
}

fn load_config(args: &ArgMatches) -> Settings {
    let path = args.value_of("CONFIG").expect("A config file is required");
    Settings::from_filepath(path)
}

fn build_query(args: &ArgMatches, config: &Settings) -> RealtorApiQuery {
    let mut query_builder = RealtorApiQuery::builder();
    query_builder = query_builder.api_key(config.api_key.as_str());
    
    if let Some(c) = args.value_of("allows-cats") {
        query_builder = query_builder.allows_cats(c.parse::<bool>().unwrap());
    }
    if let Some(c) = args.value_of("allows-dogs") {
        query_builder = query_builder.allows_dogs(c.parse::<bool>().unwrap());
    }
    if let Some(c) = args.value_of("city") {
        query_builder = query_builder.city(c);
    }
    if let Some(c) = args.values_of("features") {
        let valid = vec!["recreation_facilities", "swimming_pool", "washer_dryer", "garage_1_or_more", "central_air", "fireplace", "spa_or_hot_tub", "dishwasher", "community_doorman", "community_elevator", "furnished", "laundry_room", "community_no_fee", "community_outdoor_space", "pets_allowed"];
        let values = c.collect::<Vec<&str>>();
        let clean_vals = collect_valid_values(valid, values);
        query_builder = query_builder.features(clean_vals.as_str())
    }
    if let Some(c) = args.value_of("limit") {
        query_builder = query_builder.limit(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.value_of("min-price") {
        query_builder = query_builder.price_min(c.parse::<f32>().unwrap());
    }
    if let Some(c) = args.value_of("max-price") {
        query_builder = query_builder.price_max(c.parse::<f32>().unwrap());
    }
    if let Some(c) = args.value_of("min-sqft") {
        query_builder = query_builder.sqft_min(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.value_of("max-sqft") {
        query_builder = query_builder.sqft_max(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.value_of("min-lot-sqft") {
        query_builder = query_builder.lot_sqft_min(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.value_of("max-lot-sqft") { 
        query_builder = query_builder.lot_sqft_max(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.value_of("min-baths") {
        query_builder = query_builder.baths_min(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.value_of("min-beds") {
        query_builder = query_builder.beds_min(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.values_of("prop-type") {
        let valid = vec!["single_family", "multi_family", "condo", "mobile", "land", "farm", "other"];
        let values = c.collect::<Vec<&str>>();
        let clean_vals = collect_valid_values(valid, values);
        query_builder = query_builder.prop_type(clean_vals.as_str());
    }
    if let Some(c) = args.value_of("radius") {
        query_builder = query_builder.radius(c.parse::<u64>().unwrap());
    }
    if let Some(c) = args.value_of("sort") {
        query_builder = query_builder.sort(c);
    }
    if let Some(c) = args.value_of("state") {
        query_builder = query_builder.state_code(c);
    }
    if let Some(c) = args.value_of("zip") {
        query_builder = query_builder.postal_code(c);
    }

    query_builder.build()
}

fn notify_recipients(args: ArgMatches, config: Settings, properties: Vec<Property>) {
    if args.occurrences_of("email") > 0 {
        let body = format!("apartment-hunter found some places for you!\n\n{}", build_message(&properties));
        match notifs::email(config.email_settings, body.as_str()) {
            Ok(_) => (),
            Err(err) => panic!("Error sending email: {}", err),
        };
    }
    if args.occurrences_of("json") > 0 {
        println!("{}", serde_json::to_string(&properties).unwrap());
    }
}

fn build_message(properties: &Vec<Property>/*, template: Option<String>*/) -> String {
    //let template = match template {
    //    Some(t) => t,
    //    None => String::from(include_str!("../templates/message_template")),
    //};
    let mut result = String::from("");
    for property in properties {
        result.push_str(format!("{}\n", property).as_str());
    }

    result
}

fn collect_valid_values(valid: Vec<&str>, mut values: Vec<&str>) -> String {
    values.retain(|x| valid.contains(&x));
    let result = values.iter().fold(String::new(), |result, x| result + x + ",");
    result
}

fn get_properties(mut query: RealtorApiQuery) -> Vec<Property> {
    let mut property_set = Vec::<Property>::new();

    let client = Client::builder().build().unwrap();
    let mut response = make_request(&client, &query);
    let cap = cmp::min(response.meta.matching_rows.as_u64().unwrap(), query.limit);

    while query.offset < cap {
        if let Some(p) = response.properties {
            property_set.extend(p);
        }
        
        query.increment_offset(cmp::min(response.meta.returned_rows.as_u64().unwrap(), cap - query.offset));
        response = make_request(&client, &query);
    }

    property_set
}

fn make_request(client: &Client, query: &RealtorApiQuery) -> RealtorApiResponse {
    let host = String::from("realtor.p.rapidapi.com");
    let request = client.get(format!("https://{}{}?{}",
                       host, 
                       String::from("/properties/v2/list-for-rent"),
                       qs::to_string(&query).unwrap())
               .as_str())
        .header("x-rapidapi-key", query.api_key.trim())
        .header("x-rapidapi-host", host)
        .header("useQueryString", "true");
    
    let response = request.send().expect("Could not get a response from Realtor API");
    serde_json::from_str::<RealtorApiResponse>(response
                                             .text()
                                             .unwrap()
                                             .as_str()).unwrap()
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_collect_valid_values() {
        let valid = vec!["option-a", "option-b"];
        let vals = vec!["option-a", "option-b", "invalid-option"];
        let result = crate::collect_valid_values(valid, vals);
        assert_eq!(result, String::from("option-a,option-b,"));
    }
}
